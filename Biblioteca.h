#ifndef BIBLIOTECA_H
#define BIBLIOTECA_H

#include <iostream>
#include <list>

#include "arbre.h"
#include "Treball.h"

class Biblioteca
{
private:
	std::list<Treball> ll;

public:
	void altaTreball(Treball);
	// pre: cert
	// post: dona d'alta a un treball al sistema

	void baixaTreball(Treball);
	// pre: cert
	// post: elimina un treball de la biblioteca.

	Treball fusioTreballs(string, string);
	// pre: cert
	// post: fusiona els treballs. el nou treball te per titol el primer parametre
	// i les paraules clau del primer i del segon treball

	void llistatTreballs();
	// pre: cert
	// post: llista tots els treballs continguts en la biblioteca.

	void consultaTreball(std::string);
	// pre: cert
	// post: es dona informacio sobre un treball especific.
};

#endif
