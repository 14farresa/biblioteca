#ifndef TREBALL
#define TREBALL

#include <list>
#include <string>
#include <iostream>

using namespace std;

class Treball{
	public:
		Treball();
		/* Pre: cert */
		/* Post: el resultat es un Treball amb titol i area buits i sense paraules
		   clau */

		Treball(const string &t);
		/* Pre: cert */
		/* Post: el resultat es un Treball amb titol t, sense area ni paraules
		   clau */

		Treball(string n, list<string> p);
		//pre: cert
		//post: el treball tindra el titol del string i les paraules clau del vector.

		~Treball();
		/* Destructor per defecte */


		void afegir_titol(const string &t);
		/* Pre: cert */
		/* Post: el titol del parametre implicit passa a ser t */

		void afegir_kw(const list<string> &lkw);
		/* Pre: cert */
		/* Post: la llista de paraules clau del p. i. passa a ser lkw */

		void afegir_area(const string &a);
		/* Pre: cert */
		/* Post: l'area del parametre implicit passa a ser "a" */

		int quantes_kw() const;
		/* Pre: cert */
		/* Post: el resultat es el nombre de paraules clau del p. i. */

		bool es_kw(const string &s) const;
		/* Pre: cert */
		/* Post: el resultat indica si s es una paraula clau del p. i. */

		string consultar_titol() const;
		/* Pre: cert */
		/* Post: el resultat es el titol del p. i. */

		string consultar_area() const;
		/* Pre: cert */
		/* Post: el resultat es l'area tematica del p. i. */

		list<string> consultar_keywords() const;
		/* Pre: cert */
		/* Post: el resultat es la llista de paraules clau del p. i. */

		bool es_buit() const;
		/* Pre: cert */
		/* Post: el resultat indica si el p. i. esta buit */


		void llegir_treball();
		/* Pre: estan preparats en el canal estandard d'entrada un titol i
		   una o mes paraules clau, acabades en "." */
		/* Post: el p. i. passa a tenir els atributs llegits del canal estandard
		   d'entrada */

		void escriure_titol() const;
		/* Pre: cert */
		/* Post: s'ha escrit el titol del p. i. en el canal estandard de sortida */

		void escriure_treball() const;
		/* Pre: cert */
		/* Post: s'han escrit del dades del p. i. en el canal estandard de sortida */

	private:
		string titol;
		string area;
		list<string>  keywords;
		static void escriure_llista_kw(const list<string> &lkw);
};
#endif
