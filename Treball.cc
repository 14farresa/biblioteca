#include "Treball.h"

Treball::Treball()
{
}

Treball::Treball(const string &t)
{
	titol = t;
}

Treball::Treball(string n, list<string> p)
{
	titol = n;
	keywords = p;
}

Treball::~Treball()
{
}

void Treball::afegir_titol(const string &t)
{
	titol = t;
}

void Treball::afegir_kw(const list<string> &lkw)
{
	keywords = lkw;
}

void Treball::afegir_area(const string &a)
{
	area = a;
}

int Treball::quantes_kw() const
{
	return keywords.size();
}

bool Treball::es_kw(const string &s) const
{
	list<string>::const_iterator it = keywords.begin();
	bool trobada = false;
	while (it!=keywords.end() and not trobada){
		if (s==(*it)) trobada = true;
		else ++it;
	}
	return trobada;
}

string Treball::consultar_titol() const
{
	return titol;
}

string Treball::consultar_area() const
{
	return area;
}

list<string> Treball::consultar_keywords() const
{
	return keywords;
}

bool Treball::es_buit() const
{
	return titol.empty();
}

void Treball::llegir_treball()
{
	cin >> titol;
	list<string>::iterator it;
	string kw;
	cin >> kw;
	while (kw!=".") {
		//buscar posicio dins paraules clau per kw
		it = keywords.begin();
		bool trobada = false;
		while (it!=keywords.end() and not trobada){
			if ((*it) > kw) trobada = true;
			else ++it;
		}
		keywords.insert(it,kw);
		cin >> kw;
	}
}

void Treball::escriure_titol() const
{
	cout << titol;
}

void Treball::escriure_treball() const
{
	cout << consultar_titol() << endl;
	escriure_llista_kw(consultar_keywords());
	cout << consultar_area() << endl;
}

void Treball::escriure_llista_kw(const list<string> &lkw)
{
	for (list<string>::const_iterator it = lkw.begin(); it != lkw.end(); ++it) {
		if (it!=lkw.begin()) cout<<" ";
		cout << (*it);
	}
	cout << endl;
}
