TARGET = main
OBJS = main.o Treball.o Biblioteca.o Classificacio.o

CXX = g++
CXXFLAGS = -std=c++11 -g

all: $(OBJS)
	$(CXX) $(OBJS) -o $(TARGET)

%.o: %.cc
	$(CXX) $(CXXFLAGS) -c $< -o $@

clean:
	rm -rf $(OBJS) $(TARGET)
