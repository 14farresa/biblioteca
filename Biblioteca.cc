#include <iostream>
#include <list>

#include "arbre.h"
#include "Biblioteca.h"
#include "Treball.h"

void Biblioteca::altaTreball(Treball tr)
{
	std::list<std::string> lst = tr.consultar_keywords();
	lst.sort();
	tr.afegir_kw(lst);

	if (ll.size() == 0)
	{
		ll.push_back(tr);
	}
	else
	{
		// TODO: Comprovar majúscules i minúscules.
		bool acabat = false;
		for (std::list<Treball>::iterator it = ll.begin(); it != ll.end() && !acabat; ++it)
		{
			if (tr.consultar_area() < it->consultar_area())
			{
				ll.insert(it, tr);
				acabat = true;
			}
			else
			{
				if (tr.consultar_area() == it->consultar_area())
				{
					if (tr.consultar_titol() <= it->consultar_titol())
					{
						ll.insert(it, tr);
						acabat = true;
					}
				}
			}
		}

		if (!acabat)
		{
			ll.push_back(tr);
		}
	}
}

void Biblioteca::baixaTreball(Treball tr)
{
	bool acabat = false;
	for (std::list<Treball>::iterator it = ll.begin(); it != ll.end() && !acabat; ++it)
	{
		if (it->consultar_titol() == tr.consultar_titol())
		{
			ll.erase(it);
			acabat = true;
		}
	}
}

Treball Biblioteca::fusioTreballs(string t1, string t2)
{
    std::list<Treball>::iterator it;
    bool acabat = false;
    Treball tr1, tr2;
    for (it = ll.begin(); it != ll.end() and not acabat; ++it) {
		if (it->consultar_titol() == t1) {
			tr1 = *it;
			acabat = true;
		}
	}
	acabat = false;
    for (it = ll.begin(); it != ll.end() and not acabat; ++it) {
		if (it->consultar_titol() == t2) {
			tr2 = *it;
			acabat = true;
		}
	}

	std::list<std::string> par = tr1.consultar_keywords();
	par.merge(tr2.consultar_keywords());

	par.unique();
	Treball tr3(t1, par);

	baixaTreball(tr1);
	baixaTreball(tr2);

	return tr3;
}

void Biblioteca::llistatTreballs()
{
    cout << "Treballs" << endl;
	if (ll.size() != 0)
	{
		std::string areaActual = ll.front().consultar_area();
		std::cout << areaActual;
		for (Treball t : ll)
		{
			if (t.consultar_area() != areaActual)
			{
				areaActual = t.consultar_area();
				std::cout << std::endl << areaActual;
			}
			std::cout << " " << t.consultar_titol();
		}
	}
	cout << endl << endl;
}

void Biblioteca::consultaTreball(std::string titol)
{
	std::cout << "Consulta de treball" << std::endl;
	bool trobat = false;
	list<Treball>::iterator it = ll.begin();
	while (it != ll.end() and not trobat)
	{
		if (it->consultar_titol() == titol)
		{
			std::cout << titol  << std::endl;
			for (std::string p : it->consultar_keywords())
			{
				std::cout << p << " ";
			}
			cout << endl << it->consultar_area() << endl;
            trobat = true;
		}
		it++;
	}
	if (!trobat)
	{
		std::cout << "El treball " << titol << " no existeix" << std::endl;
	}
	std::cout << std::endl;
}
