#include "Classificacio.h"
#include "Treball.h"
#include <stack>
#include <string>
#include <vector>

using namespace std;

Classificacio::Classificacio()
{
}

void Classificacio::construeix_arbre_int()
{
	string node;
	int nf;
    stringstream ss;
	stack<arbre<string> > p;
	arbre<string> a;

	while(getline(cin, node) and not node.empty()){

	    istringstream ss(node);
	    ss >> node >> nf;

		arbre<string> fe, fd;
		if (nf != 0)
		{
			if (nf == 1)
			{ // 1 fill, l'esquerre
				fe = p.top();
				p.pop();
			}
			else
			{ // 2 fills
				fd = p.top();
				p.pop();
				fe = p.top();
				p.pop();
			}
		}
		arbre<string> a(node,fe,fd);
		p.push(a);
	}
	arb = p.top();
}

void Classificacio::list_to_InfoNode(arbre<string> &arb, const list<string> &l, vector<InfoNode> &v)
{
    list<string>::const_iterator it = l.begin();
    while (it != l.end()) {
        int prof = profunditat(arb, *it, 0);
        InfoNode info = {*it, prof};
        v.push_back(info);
        it++;
    }
}

//Pre: node € arbç

int Classificacio::profunditat(arbre<string> &arb, string node, int prof)
{
    //cout << "arrel: " << arb.arrel() << "  node: "  << node << " i prof: " << prof<<endl;
    if (arb.es_buit()) return -1;
    if (arb.arrel() == node) {
      //  cout << "retornant: "  << prof << endl;
         return prof;
    } else {
        arbre<string> fd, fe;
        fd = arb.fd();
        fe = arb.fe();

        int a1 = -1, a2 = -1;
        if (not fd.es_buit()) a1 = profunditat(fd, node, prof+1);
        if (not fe.es_buit()) a2 = profunditat(fe, node, prof+1);
        return max(a1, a2);
    }
}


//Pre: node € arb
void Classificacio::i_busca_arrel_node(arbre<string> &arb, string node, string &arrel, bool &es_fill)
{
    if (arb.arrel() == node) {
        es_fill = true;
    } else {
        if (not arb.es_buit()) {
            bool es_fill_dret = false, es_fill_esquerra = false;
            arbre<string> fd, fe;
            fd = arb.fd();
            fe = arb.fe();

            if (not fd.es_buit()) i_busca_arrel_node(fd, node, arrel, es_fill_dret);
            if (not fe.es_buit()) i_busca_arrel_node(fe, node, arrel, es_fill_esquerra);


            if (es_fill_dret or es_fill_esquerra) {
                arrel = arb.arrel();
            }
        }
    }
}


//Pre: tr te 1 paraula clau com a minim
string Classificacio::assignar_area(Treball tr)
{
    if (tr.consultar_keywords().size() == 1) {
        return tr.consultar_keywords().front();
    }

    vector<InfoNode> arrels_pclau;
    list_to_InfoNode(arb, tr.consultar_keywords(), arrels_pclau);
    int n_pclau = arrels_pclau.size();

    bool arrel_trobada = false;
    while (not arrel_trobada) {
        for (int i = 0; i < n_pclau; i++) {
            for (int j = i+1; j < n_pclau; j++) {

               // cout << "Comparant: " << arrels_pclau[i].area <<" amb " << arrels_pclau[j].area << endl;

                if (arrels_pclau[i].profunditat > arrels_pclau[j].profunditat){
                    if (arrels_pclau[i].profunditat > 0) {
                        bool b = false;
                        i_busca_arrel_node(arb, arrels_pclau[i].area, arrels_pclau[i].area, b);
                        arrels_pclau[i].profunditat--;
                    }
                } else if (arrels_pclau[i].profunditat < arrels_pclau[j].profunditat) {
                    if (arrels_pclau[j].profunditat > 0) {
                        bool b = false;
                        i_busca_arrel_node(arb, arrels_pclau[j].area, arrels_pclau[j].area, b);
                        arrels_pclau[j].profunditat--;
                    }
                } else {
                    if (arrels_pclau[i].area == arrels_pclau[j].area) {
                         arrel_trobada = true;
                    } else {
                        if (arrels_pclau[i].profunditat > 0) {
                            bool b = false;
                            i_busca_arrel_node(arb, arrels_pclau[i].area, arrels_pclau[i].area, b);
                            arrels_pclau[i].profunditat--;
                        }
                        if (arrels_pclau[j].profunditat > 0) {
                            bool b = false;
                            i_busca_arrel_node(arb, arrels_pclau[j].area, arrels_pclau[j].area, b);
                            arrels_pclau[j].profunditat--;
                        }
                    }
                }
            }
            //cout << "l'area " << arrels_pclau[i].area << " te profunditat " << arrels_pclau[i].profunditat << endl;
        }
    }
    //tots els elements del vector son iguals
    return arrels_pclau[0].area;
}

