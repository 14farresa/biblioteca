#include <iostream>
#include <list>
#include <vector>

#include "arbre.h"
#include "Treball.h"
#include <sstream>
using namespace std;

class Classificacio
{
	private:
	    struct InfoNode {
            string area;
            int profunditat;
	    };

		arbre<string> arb;
		static void i_busca_arrel_node(arbre<string> &arb, string node, string &arrel, bool &es_fill);
		/* Pre: arbre no es buit*/
		/* Post: modifica el parametre passat per referencia per tal que aquest contingui el node anterior del fill desitjat */
		static void list_to_InfoNode(arbre<string> &arb, const list<string> &l, vector<InfoNode> &v);
		/* Pre: cert */
		/* Post: passa de la llista a un vector d'structs */
		static int profunditat(arbre<string> &arb, string node, int prof);
		/* Pre: cert */
		/* Post: calcula la profunditat recursivament */

	public:
		Classificacio();
		void construeix_arbre_int();
		/* Pre: rep el node dels arbres en post ordre i on s'indica el numero de fills */
		/* Post: el resultat es un arbre binari creat a partir dels nodes rebuts */

		string assignar_area(Treball tr);
		/* Pre: el treball te paraules clau */
		/* Post: retorna l'area a la qual el treball pertany */
};
