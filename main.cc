#include <iostream>
#include <list>
using namespace std;

#include "Biblioteca.h"
#include "Treball.h"
#include "Classificacio.h"
void llegeix_opcio(int &op)
{
	cin >> op;
}
int main()
{
	Biblioteca b;
	Classificacio c;

    c.construeix_arbre_int();

    int opcio;
	llegeix_opcio(opcio);

	while (opcio != -6) {
		switch (opcio) {
		case -1: {
		    string nom_treball;
		    cin >> nom_treball;

		    list<string> pclau;
            string s;
            while (cin >> s and s != ".") {
                pclau.push_back(s);
            }
            Treball t(nom_treball, pclau);
            string area = c.assignar_area(t);
            t.afegir_area(area);
            b.altaTreball(t);
            break;
		}
		case -2: {
            string treball;
            cin >> treball;
            b.baixaTreball(treball);
			break;
		}
		case -3: {
            string t1, t2;
            cin >> t1 >> t2;

            Treball nou = b.fusioTreballs(t1,t2);
            if (nou.consultar_keywords().size()) {
                string area = c.assignar_area(nou);
                nou.afegir_area(area);
            }
            b.altaTreball(nou);
			break;
		}
		case -4: {
            b.llistatTreballs();
            break;
		}
        case -5: {
            string tr;
            cin >> tr;
            b.consultaTreball(tr);
            break;
        }
		}
		llegeix_opcio(opcio);
	}

	return 0;
}
